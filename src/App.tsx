import React from "react";
import {
  // BrowserRouter as Router,
  HashRouter,
  Switch,
  Route,
} from "react-router-dom";
import Ithenticate from "./widgets/Ithenticate";
import Workflow from "./widgets/Workflow";
import WorkflowAction from "./widgets/WorkflowAction";
import SignatureField from "./widgets/SignatureField";

function App() {
  return (
    <HashRouter>
      <Switch>
        <Route path="/ithenticate">
          <Ithenticate />
        </Route>
        <Route path="/signatureField">
          <SignatureField />
        </Route>
        <Route path="/workflow">
          <Workflow />
        </Route>
        <Route path="/workflowAction">
          <WorkflowAction />
        </Route>
        <Route path="/">
          <Home />
        </Route>
      </Switch>
    </HashRouter>
  );
}

function Home() {
  return (
    <div>
      <h2>Chegg Learn CMS Optimizations Extensions</h2>
    </div>
  );
}

export default App;
