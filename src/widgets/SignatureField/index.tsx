import React from "react";
// import moment from "moment";
import useContentfulInit from "../../hooks/useContentfulInit";
import useEntryRefresh from "../../hooks/useEntryRefresh";
// import { Link } from "contentful-ui-extensions-sdk";

function SignatureField() {
  const sdk = useContentfulInit();

  useEntryRefresh(sdk);

  // const { getUserFromSignature } = useSignature(sdk);

  // const [user, setUser] = useState<User | null>(null);

  return <p>{sdk?.field.getValue() || "Not Approved"}</p>;
}

export default SignatureField;
