import styled, { css } from "styled-components";

interface NavItemProps {
  active: boolean;
  completed?: boolean;
}

export const WizardView = styled.nav`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

export const NavItem = styled.a<NavItemProps>`
  display: block;
  flex-grow: 1;
  background: none;
  padding: 1em;
  text-align: center;
  box-shadow: inset 0px 0px 0px 1px #ababab;
  border: 1px solid white;

  ${(props) =>
    props.active
      ? css`
          background: rgb(45, 100, 179);
          color: white;
          box-shadow: none;
        `
      : ""}

  ${(props) =>
    props.completed
      ? css`
          background: #ccc;
          color: white;
          box-shadow: none;
        `
      : ""}
`;
