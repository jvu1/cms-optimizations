import React from "react";
import { WizardView, NavItem } from "./styled";
import useContentfulInit from "../../hooks/useContentfulInit";
import useEntryRefresh from "../../hooks/useEntryRefresh";
import useWorkflowState, { WorkflowStates } from "../../hooks/useWorkflowState";

export type Step = {
  state: WorkflowStates;
  label: string;
};

const steps: Step[] = [
  {
    state: WorkflowStates.Authoring,
    label: "Editing",
  },
  {
    state: WorkflowStates.PlagerismCheck,
    label: "Plagerism check",
  },
  {
    state: WorkflowStates.CopyEdit,
    label: "Copy edit",
  },
  {
    state: WorkflowStates.QAReview,
    label: "QA Review",
  },
  {
    state: WorkflowStates.Done,
    label: "Finished",
  },
];

function Workflow() {
  const sdk = useContentfulInit();

  useEntryRefresh(sdk);

  const { getWorkflowState } = useWorkflowState(sdk);

  const currentState = getWorkflowState();

  let showCompleted = true;

  return (
    <WizardView>
      {steps.map((step) => {
        const { label, state } = step;

        let isActive = false;
        if (currentState === state) {
          isActive = true;
          showCompleted = false;
        }

        return (
          <NavItem active={isActive} completed={showCompleted}>
            {label}
          </NavItem>
        );
      })}
    </WizardView>
  );
}

export default Workflow;
