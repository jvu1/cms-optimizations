import styled from "styled-components";
import { EntryStatusEnum } from "./types";

type TagProps = {
  status: EntryStatusEnum | null;
};

export const WorkflowActionContainer = styled.section``;

export const ActionButton = styled.button`
  display: block;
  width: 100%;
  padding: 1em;
  background: rgb(22, 135, 93);
  border: none;
  color: white;
  font-size: 1em;
  font-weight: bold;
  cursor: pointer;

  &:hover {
    background: #1a6848;
  }
`;

export const SecondaryButton = styled.button`
  display: block;
  width: 100%;
  padding: 1em;
  background: #e5ebed;
  border: 1px solid #ccc;
  color: black;
  font-size: 1em;
  font-weight: normal;
  cursor: pointer;

  &:hover {
    background: #ccc;
  }
`;

export const Flex = styled.div`
  display: flex;
  padding-top: 1em;
`;

export const Tag = styled.span<TagProps>`
  text-transform: uppercase;
  font-weight: bold;
  font-size: 0.75em;
  letter-spacing: 1.6px;

  color: ${(props) => {
    if (props.status === EntryStatusEnum.Draft) {
      return `orange`;
    }
    if (props.status === EntryStatusEnum.Changed) {
      return "blue";
    }
    return "green";
  }};
`;
