import React, { useState, useEffect } from "react";
import moment from "moment";
import { ControlsProps } from "./types";

function LastUpdated(props: ControlsProps) {
  const entry = props.sdk?.entry;

  const [count, setCount] = useState<number>(0);

  useEffect(() => {
    if (entry) {
      const t = setInterval(() => {
        setCount(count + 1);
      }, 5000);

      return () => {
        clearInterval(t);
      };
    }
    return;
  }, [count, entry]);

  if (!entry) return <p>Loading...</p>;

  const updatedAt = entry.getSys().updatedAt;

  const timeFromNow = moment(new Date(updatedAt)).fromNow();

  return <p>Last saved {timeFromNow}</p>;
}

export default LastUpdated;
