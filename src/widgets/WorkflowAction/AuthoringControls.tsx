import React, { useCallback, useState } from "react";
import { ControlsProps } from "./types";
import { ActionButton } from "./styled";
import useWorkflowState, { WorkflowStates } from "../../hooks/useWorkflowState";
import useSignature, { UserSignature } from "../../hooks/useSignature";

function AuthoringControls(props: ControlsProps) {
  const { sdk } = props;

  const [loading, setLoading] = useState(false);
  const {
    resetWorkflow,
    signIt,
    setWorkflowState,
    saveEntry,
  } = useWorkflowState(sdk);
  const { getCurrentUserSignature } = useSignature(sdk);

  const handleClick = useCallback(() => {
    setLoading(true);

    // Reset fields
    resetWorkflow(WorkflowStates.Authoring);

    // Sign Author approval
    signIt(
      WorkflowStates.Authoring,
      getCurrentUserSignature() as UserSignature
    );

    // Set Next State
    setWorkflowState(WorkflowStates.PlagerismCheck);

    // Save it
    saveEntry(() => {
      setLoading(false);
      console.log("yay");
    });
  }, []);

  return loading ? (
    <div>Loading...</div>
  ) : (
    <div>
      <ActionButton onClick={handleClick}>Submit</ActionButton>
    </div>
  );
}

export default AuthoringControls;
