import React from "react";
import useContentfulInit from "../../hooks/useContentfulInit";
import useWorkflowState, { WorkflowStates } from "../../hooks/useWorkflowState";
import AuthoringControls from "./AuthoringControls";
import PlagerismCheckControls from "./PlagerismCheckControls";
import CopyEditControls from "./CopyEditControls";
import QAReviewControls from "./QAReviewControls";
import EntryStatus from "./EntryStatus";
import LastUpdated from "./LastUpdated";
import DoneControls from "./DoneControls";
import { ControlsProps } from "./types";
import useEntryRefresh from "../../hooks/useEntryRefresh";
import { WorkflowActionContainer } from "./styled";

const stateMapComponent = {
  [WorkflowStates.Authoring]: AuthoringControls,
  [WorkflowStates.PlagerismCheck]: PlagerismCheckControls,
  [WorkflowStates.CopyEdit]: CopyEditControls,
  [WorkflowStates.QAReview]: QAReviewControls,
  [WorkflowStates.Done]: DoneControls,
};

function WorkflowAction() {
  const sdk = useContentfulInit();
  useEntryRefresh(sdk);
  const { getWorkflowState } = useWorkflowState(sdk);

  const currentState = getWorkflowState();

  const Component = stateMapComponent[currentState] as React.ComponentType<
    ControlsProps
  >;

  return sdk?.entry ? (
    <WorkflowActionContainer>
      <EntryStatus sdk={sdk} />
      <Component sdk={sdk} />
      <LastUpdated sdk={sdk} />
    </WorkflowActionContainer>
  ) : (
    <>Loading...</>
  );
}

export default WorkflowAction;
