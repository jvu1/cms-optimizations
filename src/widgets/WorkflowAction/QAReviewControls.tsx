import React, { useCallback, useState } from "react";
import { ControlsProps } from "./types";
import { ActionButton, Flex, SecondaryButton } from "./styled";
import useWorkflowState, { WorkflowStates } from "../../hooks/useWorkflowState";
import useSignature, { UserSignature } from "../../hooks/useSignature";

function QAReviewControls(props: ControlsProps) {
  const { sdk } = props;

  const [loading, setLoading] = useState(false);
  const {
    resetWorkflow,
    saveEntry,
    signIt,
    setWorkflowState,
  } = useWorkflowState(sdk);
  const { getCurrentUserSignature } = useSignature(sdk);

  const handleClickGoToAuthoring = useCallback(() => {
    setLoading(true);

    // Reset fields
    resetWorkflow(WorkflowStates.Authoring);

    // Set Next State
    setWorkflowState(WorkflowStates.Authoring);

    // TODO: Assign task

    // Save it
    saveEntry(() => {
      setLoading(false);
      console.log("yay");
    });
  }, []);

  const handleClickGoToCopyEdit = useCallback(() => {
    setLoading(true);

    // Reset fields
    resetWorkflow(WorkflowStates.CopyEdit);

    // Set Next State
    setWorkflowState(WorkflowStates.CopyEdit);

    // TODO: Assign task

    // Save it
    saveEntry(() => {
      setLoading(false);
      console.log("yay");
    });
  }, []);

  const handleClickGoToDone = useCallback(() => {
    setLoading(true);

    // Reset fields
    resetWorkflow(WorkflowStates.QAReview);

    // Sign Author approval
    signIt(WorkflowStates.QAReview, getCurrentUserSignature() as UserSignature);

    // Set Next State
    setWorkflowState(WorkflowStates.Done);

    // Save it
    saveEntry(() => {
      setLoading(false);
      console.log("yay");
    });
  }, []);

  return loading ? (
    <div>Loading...</div>
  ) : (
    <div>
      <ActionButton onClick={handleClickGoToDone}>Done</ActionButton>
      <Flex>
        <SecondaryButton onClick={handleClickGoToCopyEdit}>
          &lt; Copy Edit
        </SecondaryButton>
        <SecondaryButton onClick={handleClickGoToAuthoring}>
          &lt;&lt; Rewrite
        </SecondaryButton>
      </Flex>
    </div>
  );
}

export default QAReviewControls;
