import React, { useState, useEffect } from "react";
import { ControlsProps } from "./types";
import useEntryStatus from "../../hooks/useEntryStatus";
import { Tag } from "./styled";

function EntryStatus(props: ControlsProps) {
  const entry = props.sdk?.entry;

  const [count, setCount] = useState<number>(0);

  useEffect(() => {
    if (entry) {
      const t = setInterval(() => {
        setCount(count + 1);
      }, 5000);

      return () => {
        clearInterval(t);
      };
    }
    return;
  }, [count, entry]);

  const status = useEntryStatus(props.sdk);

  if (!entry) return <p>Loading...</p>;

  return (
    <p>
      Current Status: <Tag status={status}>{status}</Tag>
    </p>
  );
}

export default EntryStatus;
