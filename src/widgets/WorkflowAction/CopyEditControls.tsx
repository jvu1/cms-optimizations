import React, { useCallback, useState } from "react";
import { ControlsProps } from "./types";
import { ActionButton, Flex, SecondaryButton } from "./styled";
import useWorkflowState, { WorkflowStates } from "../../hooks/useWorkflowState";
import useSignature, { UserSignature } from "../../hooks/useSignature";

function CopyEditControls(props: ControlsProps) {
  const { sdk } = props;

  const [loading, setLoading] = useState(false);
  const {
    resetWorkflow,
    saveEntry,
    signIt,
    setWorkflowState,
  } = useWorkflowState(sdk);
  const { getCurrentUserSignature } = useSignature(sdk);

  const handleClickGoToAuthoring = useCallback(() => {
    setLoading(true);

    // Reset fields
    resetWorkflow(WorkflowStates.Authoring);

    // Set Next State
    setWorkflowState(WorkflowStates.Authoring);

    // TODO: Assign task

    // Save it
    saveEntry(() => {
      setLoading(false);
      console.log("yay");
    });
  }, []);

  const handleClickGoToQA = useCallback(() => {
    setLoading(true);

    // Reset fields
    resetWorkflow(WorkflowStates.CopyEdit);

    // Sign Author approval
    signIt(WorkflowStates.CopyEdit, getCurrentUserSignature() as UserSignature);

    // Change appropriate field
    setWorkflowState(WorkflowStates.QAReview);

    // Save it
    saveEntry(() => {
      setLoading(false);
      console.log("yay");
    });
  }, []);

  return loading ? (
    <div>Loading...</div>
  ) : (
    <div>
      <Flex>
        <SecondaryButton onClick={handleClickGoToAuthoring}>
          &lt; Rewrite
        </SecondaryButton>
        <ActionButton onClick={handleClickGoToQA}>QA Review &gt;</ActionButton>
      </Flex>
    </div>
  );
}

export default CopyEditControls;
