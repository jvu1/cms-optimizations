import React, { useCallback, useState } from "react";
import { ControlsProps } from "./types";
import { ActionButton, Flex, SecondaryButton } from "./styled";
import useWorkflowState, { WorkflowStates } from "../../hooks/useWorkflowState";

function DoneControls(props: ControlsProps) {
  const { sdk } = props;

  const [loading, setLoading] = useState(false);
  const {
    resetWorkflow,
    saveEntry,
    publishEntry,
    setWorkflowState,
  } = useWorkflowState(sdk);

  const handlePublish = useCallback(() => {
    // Save it
    publishEntry(() => {
      console.log("yay");
    });
  }, []);

  const handleClickGoToAuthoring = useCallback(() => {
    setLoading(true);

    // Reset fields
    resetWorkflow(WorkflowStates.Authoring);

    // Set Next State
    setWorkflowState(WorkflowStates.Authoring);

    // TODO: Assign task

    // Save it
    saveEntry(() => {
      setLoading(false);
      console.log("yay");
    });
  }, []);

  return loading ? (
    <div>Loading...</div>
  ) : (
    <div>
      <ActionButton onClick={handlePublish}>Publish</ActionButton>
      <Flex>
        <SecondaryButton onClick={handleClickGoToAuthoring}>
          &lt; Rewrite
        </SecondaryButton>
      </Flex>
    </div>
  );
}

export default DoneControls;
