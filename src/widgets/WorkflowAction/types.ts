import { FieldExtensionSDK } from "contentful-ui-extensions-sdk";

export interface ControlsProps {
  sdk?: FieldExtensionSDK;
}

export enum EntryStatusEnum {
  Draft = "draft",
  Changed = "changed",
  Published = "published",
}
