import React, { useEffect, useState } from "react";
import { init as initContentfulExtension } from "contentful-ui-extensions-sdk";
import Login from "./Login";
import Home from "./Home";

function Router() {
  const [page, setPage] = useState("Login");

  useEffect(() => {
    initContentfulExtension((api) => {
      api.window.updateHeight();
    });
  }, []);

  if (page === "Login") return <Login onNavigate={setPage} />;
  if (page === "Home") return <Home onNavigate={setPage} />;

  return <></>;
}

export default Router;
