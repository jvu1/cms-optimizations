import React, { useEffect, useState } from "react";
import { init as initContentfulExtension } from "contentful-ui-extensions-sdk";
import { documentToHtmlString } from "@contentful/rich-text-html-renderer";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import {
  getFolder,
  submitDocument,
  pollingReportStatus,
  removeTags,
  getReport,
  findFile,
} from "../api";

async function getHtml(sdk, entryId) {
  const doc = await sdk.space.getEntry(entryId);
  return {
    title: doc.fields.title && doc.fields.title["en-US"],
    body: doc.fields.body ? documentToHtmlString(doc.fields.body["en-US"]) : "",
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      marginBottom: theme.spacing(2),
      width: "100%",
    },
  },
}));

function Home() {
  const classes = useStyles();

  const [workflowState, setWorkflowState] = useState("init");
  const [folderData, setFolderData] = useState(null);
  const [currentVersion, setCurrentVersion] = useState();
  const [finalDocReport, setShowReport] = useState(null);
  const [fileName, setFileName] = useState("");
  const [file, setFile] = useState(null);
  const [reportUrl, setReportUrl] = useState(null);

  useEffect(() => {
    initContentfulExtension((sdk) => {
      const { version } = sdk.entry.getSys();
      setCurrentVersion(version);

      if (!fileName) {
        setFileName(`${sdk.entry.fields.cmsName.getValue()}--${version}`);
      }

      if (!folderData) {
        getFolder("My Documents").then((folderData) => {
          setFolderData(folderData);
        });
      }

      if (!file && folderData && fileName) {
        findFile(folderData, fileName).then((foundFile) => {
          setFile(foundFile);
        });
      }

      if (folderData && fileName && file && !finalDocReport) {
        pollingReportStatus(folderData, fileName, 1000, 300000)
          .then((finalDocReport) => {
            setShowReport(finalDocReport);
            return getReport(finalDocReport);
          })
          .then((url) => {
            console.log("url", url);
            setReportUrl(url);
          });
      }

      sdk.window.updateHeight();
    });
  }, [
    workflowState,
    file,
    fileName,
    folderData,
    finalDocReport,
    reportUrl,
    // change,
  ]);

  const handleSubmit = () => {
    // Gather api
    initContentfulExtension(async (sdk) => {
      // Get Data
      // -- compose html
      const { title, definition, overview, bodySections } = sdk.entry.fields;

      const titleText = title.getValue();

      const { body: definitionHtml } = await getHtml(
        sdk,
        definition.getValue().sys.id
      );

      const { title: overviewTitle, body: overviewHtml } = await getHtml(
        sdk,
        overview.getValue().sys.id
      );

      const bodySectionsDoc = await (bodySections.getValue() &&
      bodySections.getValue().length > 0
        ? Promise.all(
            bodySections.getValue().map((section) => {
              return getHtml(sdk, section.sys.id);
            })
          )
        : Promise.resolve([]));

      const content = `
        <!DOCTYPE html>
        <html>
          <head>
            <title>${titleText}</title>
          </head>
          <body>
            <h1>${titleText}</h1>

            <section>
              Definition: ${removeTags(definitionHtml)}
            </section>

            <section>
              <h2>${overviewTitle}</h2>
              <p>
                ${removeTags(overviewHtml)}
              </p>
            </section>

            ${
              bodySectionsDoc.length &&
              bodySectionsDoc
                .map((section) => {
                  const { title, body } = section;
                  return `
                <section>
                  <h2>${title}</h2>
                  <p>
                    ${removeTags(body)}
                  </p>
                </section>
              `;
                })
                .join("")
            }
          </body>
        </html>
      `;

      /**
       * Submit Document
       */
      await submitDocument(folderData, fileName, content.toString());

      setWorkflowState("submitted");
    });
  };

  if (!file) {
    return (
      <div className={classes.form}>
        <p>Begin the process for this revision {currentVersion}</p>
        <Button variant="contained" color="primary" onClick={handleSubmit}>
          Click to start
        </Button>
      </div>
    );
  }

  if (!finalDocReport) {
    return <div>Loading</div>;
  }

  const { score } = finalDocReport.parts ? finalDocReport.parts[0] : {};

  return score ? (
    <div className={classes.form}>
      <p>Score for version {currentVersion}</p>
      <p>{score}%</p>
      {reportUrl && (
        <a
          href={reportUrl.view_only_url}
          target="_blank"
          rel="noopener noreferrer"
        >
          See full report here
        </a>
      )}
    </div>
  ) : (
    <></>
  );
}

export default Home;
