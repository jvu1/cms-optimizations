import React, { useState, useEffect } from "react";
import { init as initContentfulExtension } from "contentful-ui-extensions-sdk";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";

import { login, checkValidSid } from "../api";

const useStyles = makeStyles((theme) => ({
  form: {
    "& > *": {
      marginBottom: theme.spacing(2),
      width: "100%",
    },
  },
}));

function Login(props) {
  const { onNavigate } = props;
  const classes = useStyles();

  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  useEffect(() => {
    checkValidSid().then((valid) => {
      if (valid) {
        return onNavigate("Home");
      }
    });

    initContentfulExtension((api) => {
      api.window.updateHeight();
    });
  }, [onNavigate]);

  const handleSubmit = async () => {
    await login(username, password);
    onNavigate("Home");
  };

  return (
    <form className={classes.form} noValidate autoComplete="off">
      <TextField
        label="Email"
        onChange={(e) => {
          const value = e.currentTarget.value;
          setUsername(value);
        }}
      />
      <TextField
        label="Password"
        type="password"
        autoComplete="current-password"
        onChange={(e) => {
          const value = e.currentTarget.value;
          setPassword(value);
        }}
      />
      <Button variant="contained" color="primary" onClick={handleSubmit}>
        Login
      </Button>
    </form>
  );
}

export default Login;
