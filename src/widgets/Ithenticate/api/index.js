import { ApolloClient, InMemoryCache, gql } from "@apollo/client";

const client = new ApolloClient({
  uri: "https://trunk.live.test.cheggnet.com/learn-bff",
  cache: new InMemoryCache(),
});

function log(...args) {
  console.log(...args);
}

function request(action, params) {
  return client
    .query({
      query: gql`
        query IthenticateQuery($action: String!, $params: IthenticateScalar!) {
          ithenticateClient(action: $action, params: $params)
        }
      `,
      variables: {
        action,
        params,
      },
    })
    .then((res) => {
      return res.data.ithenticateClient;
    });
}

function upload(action, params, encoding) {
  console.log("params", params);

  return client
    .mutate({
      mutation: gql`
        mutation IthenticateUpload(
          $action: String!
          $params: IthenticateScalar!
          $encoding: EncodingType!
        ) {
          ithenticateUpload(
            action: $action
            params: $params
            encoding: $encoding
          )
        }
      `,
      variables: {
        action,
        params,
        encoding,
      },
    })
    .then((res) => {
      return res.data.ithenticateClient;
    });
}

function setState(key, value) {
  localStorage.setItem(key, value);
}

function getState(key) {
  return localStorage.getItem(key);
}

export function removeTags(str) {
  if (str === null || str === "") return false;
  else str = str.toString();
  return str.replace(/<\/([^>]*)>/gi, "\n").replace(/(<([^>]+)>)/gi, "");
}

export async function login(username, password) {
  const data = await request("login", {
    username,
    password,
  });

  const { sid } = data;

  /**
   * Store to localstorage
   */
  setState("username", username);
  setState("sid", sid);

  log(">> sid", sid);

  return sid;
}

export async function checkValidSid() {
  const sid = getState("sid");

  if (!sid) {
    return false;
  }

  try {
    const folderListData = await request("folder.list", {
      sid,
    });

    console.log("folderListData 111", folderListData.status === 401);

    if (folderListData.status === 401) {
      setState("sid", null);
      return false;
    }

    return true;
  } catch (e) {
    console.log("e", e);
    setState("sid", null);
    return false;
  }
}

export async function getFolder(folderName) {
  const sid = getState("sid");

  console.log("sid", sid);

  /**
   * Find "My Documents" Folder
   */
  const folderListData = await request("folder.list", {
    sid,
  });

  const myFolder = folderListData.folders.find(
    (folder) => folder.name === folderName
  );

  log(">> myFolderFromList", myFolder);

  /**
   * Get detailed folder information
   */
  const folderData = await request("folder.get", {
    sid,
    id: myFolder.id,
  });

  log(">> myFolderDetails", folderData);

  return folderData;
}

export async function submitDocument(folderData, title, content) {
  const sid = getState("sid");
  const username = getState("username");
  const folderId = folderData.folder.id;

  /**
   * Upload/Submit document
   */
  const documentData = await upload(
    "document.add",
    {
      sid,
      folder: folderId,
      submit_to: 1,
      uploads: [
        {
          mime_type: "text/html",
          filename: "/tmp/CVxcXdk3lT.html",
          author_first: username,
          author_last: "",
          upload: content,
          title,
        },
      ],
    },
    "ASCII"
  );

  log(">> documentData", documentData);
}

export async function findFile(folderData, title) {
  const newFolderData = await getFolder(folderData.folder.name);
  return newFolderData.documents.find((doc) => doc.title === title);
}

export async function pollingReportStatus(
  folderData,
  title,
  pollingIntervalTime,
  timeLeft
) {
  return new Promise(async (resolve, reject) => {
    if (timeLeft <= 0) reject(new Error("Timed out!"));

    console.log(">>>>> Report Pending....");

    const sid = getState("sid");

    const newFolderData = await getFolder(folderData.folder.name);

    log(">>>> docu", newFolderData);

    const document = newFolderData.documents.find((doc) => doc.title === title);

    if (!document) {
      return setTimeout(() => {
        resolve(
          pollingReportStatus(
            newFolderData,
            title,
            pollingIntervalTime,
            timeLeft - pollingIntervalTime
          )
        );
      }, pollingIntervalTime);
    }

    /**
     * Get Document status
     */
    const status = await request("document.get", {
      sid,
      id: document.id,
    });

    log(">> status", status);

    const documentReport = status.documents[0];

    log(">> documentReport", documentReport);

    if (documentReport.is_pending) {
      return setTimeout(() => {
        resolve(
          pollingReportStatus(
            folderData,
            title,
            pollingIntervalTime,
            timeLeft - pollingIntervalTime
          )
        );
      }, pollingIntervalTime);
    }

    log(">> final documentReport", documentReport);
    resolve(documentReport);
  });
}

export async function getReport(document) {
  const sid = getState("sid");

  /**
   * Get Similarity report
   */
  const report = await request("report.get_document", {
    sid,
    id: document.id,
  });

  log(">> report", report);

  return report;
}
