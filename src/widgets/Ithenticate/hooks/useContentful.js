import { useState, useEffect } from "react";
import { init as initContentfulExtension } from "contentful-ui-extensions-sdk";

export default function useContentful() {
  const [currentApi, setCurrentApi] = useState(null);

  useEffect(() => {
    initContentfulExtension((api) => {
      setCurrentApi(api);
    });
  }, []);

  return currentApi;
}
