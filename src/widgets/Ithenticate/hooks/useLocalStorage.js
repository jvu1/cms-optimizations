export default function useLocalStorage(key) {
  const currentState = localStorage.getItem(key);

  const setLocalStorage = (value) => {
    return localStorage.setItem(key, value);
  };

  return [currentState, setLocalStorage];
}
