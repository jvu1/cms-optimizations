import React from "react";
import Router from "./components/Router";
import useContentfulInit from "../../hooks/useContentfulInit";
import useEntryRefresh from "../../hooks/useEntryRefresh";
import useWorkflowState, { WorkflowStates } from "../../hooks/useWorkflowState";

const Ithenticate = () => {
  const sdk = useContentfulInit();
  useEntryRefresh(sdk);
  const { getWorkflowState } = useWorkflowState(sdk);

  const currentState = getWorkflowState();

  const showIthenticate = currentState === WorkflowStates.PlagerismCheck;

  return showIthenticate ? <Router /> : <div></div>;
};

export default Ithenticate;
