import { FieldExtensionSDK, User } from "contentful-ui-extensions-sdk";

export type UserSignature = {
  user: User;
  timestamp: string;
};

// async function fetchUsers(
//   sdk: FieldExtensionSDK
// ): Promise<CollectionResponse<User>> {
//   const response: CollectionResponse<User> = await sdk?.space.getUsers();
//   return response;
// }

// async function fetchUser(
//   sdk: FieldExtensionSDK,
//   userLink: Link,
//   callback?: Function
// ): Promise<User | undefined> {
//   const id = userLink.sys.id;
//   const allUsers = await fetchUsers(sdk);
//   const foundUser = allUsers?.items.find((user: User) => user.sys.id === id);
//   if (callback) callback(foundUser);
//   return foundUser;
// }

const useSignature = (sdk: FieldExtensionSDK | undefined | null) => {
  /**
   * Get current user
   * @type {User}
   */
  const currentUser: User | undefined = sdk?.user;

  /**
   * Get Current User Signature
   * @returns {UserSignature}
   */
  const getCurrentUserSignature = (): UserSignature | undefined => {
    return currentUser
      ? {
          user: currentUser,
          timestamp: new Date().toISOString(),
        }
      : undefined;
  };

  return {
    currentUser,
    getCurrentUserSignature,
  };
};

export default useSignature;
