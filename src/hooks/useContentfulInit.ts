import { useEffect, useState } from "react";
import {
  init as initContentfulExtension,
  FieldExtensionSDK,
} from "contentful-ui-extensions-sdk";

/**
 *
 * @param {() => {}[]} callbacks array of function callbacks
 */
const useContentfulInit = (): FieldExtensionSDK | null => {
  const [sdkState, setSdkState] = useState<FieldExtensionSDK | null>(null);

  useEffect(() => {
    if (!sdkState) {
      initContentfulExtension((sdk: FieldExtensionSDK) => {
        setSdkState(sdk);
        sdk.window.startAutoResizer();
      });
    }
  }, [sdkState]);

  return sdkState;
};

export default useContentfulInit;
