import { FieldExtensionSDK } from "contentful-ui-extensions-sdk";
import moment from "moment";

export enum EntryStatusEnum {
  Draft = "draft",
  Changed = "changed",
  Published = "published",
}

const useEntryStatus = (sdk: FieldExtensionSDK | null | undefined) => {
  const entry = sdk?.entry;

  if (!entry) return null;

  const { publishedAt, updatedAt } = entry.getSys();

  let status = EntryStatusEnum.Draft;
  if (publishedAt) {
    status = EntryStatusEnum.Published;

    const publishedAtDate = new Date(publishedAt);
    const updatedAtDate = new Date(updatedAt);

    if (moment(updatedAtDate).isAfter(publishedAtDate)) {
      status = EntryStatusEnum.Changed;
    }
  }

  return status;
};

export default useEntryStatus;
