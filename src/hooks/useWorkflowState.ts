import { FieldExtensionSDK, EntryFieldAPI } from "contentful-ui-extensions-sdk";
import { UserSignature } from "./useSignature";
import useEntryStatus, { EntryStatusEnum } from "./useEntryStatus";

export enum WorkflowStates {
  Authoring = "authoring",
  PlagerismCheck = "plagerism-check",
  CopyEdit = "copy-edit",
  QAReview = "qa-review",
  Done = "done",
}

export type WorkflowObject = {
  authorApproved?: UserSignature;
  plagerismCheckApproved?: UserSignature;
  copyEditApproved?: UserSignature;
  qaReviewApproved?: UserSignature;
};

export type FieldMapItem = {
  state: WorkflowStates;
  field: string;
  approvedField: keyof WorkflowObject;
};

export const fieldMap: FieldMapItem[] = [
  {
    state: WorkflowStates.Authoring,
    field: "workflowAuthoredBy",
    approvedField: "authorApproved",
  },
  {
    state: WorkflowStates.PlagerismCheck,
    field: "workflowPlagerismCheckedBy",
    approvedField: "plagerismCheckApproved",
  },
  {
    state: WorkflowStates.CopyEdit,
    field: "workflowCopyEditedBy",
    approvedField: "copyEditApproved",
  },
  {
    state: WorkflowStates.QAReview,
    field: "workflowQaReviewedBy",
    approvedField: "qaReviewApproved",
  },
];

export type WorkflowHandlers = {
  signIt: (state: WorkflowStates, sign: UserSignature) => void;
  getWorkflowState: () => WorkflowStates;
  setWorkflowState: (state: WorkflowStates) => void;
  resetWorkflow: (state: WorkflowStates) => void;
  saveEntry: (callback?: Function) => void;
  publishEntry: (callback?: Function) => void;
};

const useWorkflowState = (
  sdk: FieldExtensionSDK | null | undefined
): WorkflowHandlers => {
  const entryFields = sdk?.entry?.fields;

  // State Handlers
  const signIt = (state: WorkflowStates, sign: UserSignature) => {
    const { user } = sign;

    const { field, approvedField } = fieldMap.find(
      (item) => item.state === state
    ) as FieldMapItem;

    if (entryFields) {
      // Set field
      (entryFields[field] as EntryFieldAPI).setValue(user.email);

      // Set Workflow object
      const workflowObj: WorkflowObject = entryFields.workflow.getValue() || {};
      workflowObj[approvedField] = sign;
      (entryFields.workflow as EntryFieldAPI).setValue(workflowObj);
    }
  };

  const resetWorkflow = (state?: WorkflowStates) => {
    let toReset = false;
    fieldMap.forEach((item) => {
      const { state: itemState, field } = item;

      if (state && itemState === state) {
        toReset = true;
      }

      if (toReset && entryFields) {
        (entryFields[field] as EntryFieldAPI).removeValue();
      }
    });
  };

  const setWorkflowState = (state: WorkflowStates) => {
    entryFields?.workflowState?.setValue(state);
  };

  const getWorkflowState = (): WorkflowStates => {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const status = useEntryStatus(sdk);

    // If theres no value, then infer
    if (!entryFields?.workflowState?.getValue()) {
      if (status === EntryStatusEnum.Draft) {
        return WorkflowStates.Authoring;
      }
      if (status === EntryStatusEnum.Published) {
        return WorkflowStates.Done;
      }
      return WorkflowStates.QAReview;
    }

    return entryFields?.workflowState?.getValue();
  };

  const saveEntry = async (callback?: Function) => {
    // Get Entry
    const entryId = sdk?.entry?.getSys().id;
    const entry = await sdk?.space.getEntry(entryId as string);

    // Check if user can update
    const canUpdate = await sdk?.access.can("update", entry); // -> can a user update this specific entry?

    if (!canUpdate) {
      if (callback) callback("Cannot update");
    } else {
      // Save it
      const result = await sdk?.space.updateEntry(entry);

      // Callback
      if (callback) callback(result);
    }
  };

  const publishEntry = async (callback?: Function) => {
    // Get Entry
    const entryId = sdk?.entry?.getSys().id;
    const entry = await sdk?.space.getEntry(entryId as string);

    // Check if user can publish
    const canPublish = await sdk?.access.can("publish", entry); // -> can a user update this specific entry?

    if (!canPublish) {
      if (callback) callback("Cannot publish");
    } else {
      // Publish it
      const result = await sdk?.space.publishEntry(entry);

      // Callback
      if (callback) callback(result);
    }
  };

  return {
    getWorkflowState,
    setWorkflowState,
    signIt,
    resetWorkflow,
    saveEntry,
    publishEntry,
  };
};

export default useWorkflowState;
