import { useEffect, useState } from "react";
import { FieldExtensionSDK } from "contentful-ui-extensions-sdk";

const useEntryRefresh = (sdk: FieldExtensionSDK | undefined | null) => {
  const [updatedAtState, setUpdatedAtState] = useState<string | null>(null);

  useEffect(() => {
    if (sdk?.entry) {
      const detachSysChangedHandler = sdk?.entry.onSysChanged((data) => {
        const { updatedAt } = data;
        setUpdatedAtState(updatedAt);
      });

      return () => {
        detachSysChangedHandler();
      };
    }
    return;
  }, [sdk, updatedAtState]);

  return updatedAtState;
};

export default useEntryRefresh;
